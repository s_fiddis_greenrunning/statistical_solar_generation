"""The PowerPredictor class takes the power data measured from a solar panel
and uses it as the ground truth to preform solar energy prediction. The
prediction is made by taking the ground level solar irradiance for a given day,
latitude and longigude as well as the cloud and rain forecasts from MetOffice
images and combining them using a number of variables and the cvxpy optimizer.
"""

from solar_irradiance import SolarIrradiance
from weather_prediction import WeatherPredictor
import mysql.connector
from datetime import datetime, timedelta
import urllib
import json
import pytz
import pandas as pd
import operator
import matplotlib.pyplot as plt

import cvxpy as cvx

class PowerPredictor(object):
    def __init__(self, day, lat, long, mac, interval='60min', day_range=5):
        if isinstance(day, str):
            self.day = datetime.strptime(day, '%Y-%m-%d')
        else:
            self.day = day
        self.lat = lat
        self.long = long
        self.mac = mac
        self.interval = interval
        self.day_range = day_range
        self.SI = SolarIrradiance()
        self.WP = WeatherPredictor()

        port=3306
        database='hackneydb'
        user='tradingdbroot'
        host='tradingdb.cfcv9idgwz7v.eu-west-2.rds.amazonaws.com'
        passwd='KBJZmNjMmAgBVzA9'

        self.DB = Database(host, port, user, passwd, database)
        self.initialize_dataframe()

    def initialize_dataframe(self):
        start_time = self.day.replace(tzinfo=pytz.utc)
        end_time = start_time + timedelta(self.day_range)
        self.df = pd.DataFrame(index=pd.date_range(start_time, end_time, freq=self.interval), columns=['Solar','Cloud','Rain','Ground','Power'])
        for i in range(self.day_range):
            day = self.day + timedelta(i)
            self.df.update(self.SI.produce_solar_dataframe(day, self.lat, self.long, self.interval)['Solar'].shift(1).fillna(0))
            self.df.update(self.WP.get_interpolated_dataframe(day, self.lat, self.long, 'Cloud', self.interval)['Cloud'])
            self.df.update(self.WP.get_interpolated_dataframe(day, self.lat, self.long, 'Rain', self.interval)['Rain'])
            self.df.update(self.DB.collect_data(self.mac, day, self.interval)['Ground'])
        self.df['Power'] = self.df['Solar']

    def setup_dummy_ground(self, scale_var, scale_sqr_var, cloud_var, cloud_sqr_var, add_var):
        self.df['Ground'] = self.df['Power'] * (scale_var + self.df['Cloud'] * cloud_var + self.df['Cloud'] ** 2 * cloud_sqr_var) + add_var + self.df['Power'] ** 2 * scale_sqr_var

    def run_cvx_solver(self):
        scale_var = cvx.Variable()
        scale_sqr_var = cvx.Variable()
        cloud_var = cvx.Variable()
        cloud_sqr_var = cvx.Variable()
        add_var = cvx.Variable()

        solvers = [cvx.OSQP, cvx.ECOS, cvx.ECOS_BB]
        solver_int = 0

        power_scale = scale_var * self.df['Power'].tolist()
        power_sqr_scale = scale_sqr_var * (self.df['Power'] ** 2).tolist()
        cloud_scale = (self.df['Power'] * self.df['Cloud']).tolist() * cloud_var
        cloud_sqr_scale = (self.df['Power'] * (self.df['Cloud'] ** 2)).tolist() * cloud_sqr_var

        objective_func = cvx.Minimize(cvx.sum((cvx.abs(
        power_scale
        + power_sqr_scale
        + cloud_scale
        + cloud_sqr_scale
        + add_var
        - self.df['Ground'].tolist()))**2))

        prob = cvx.Problem(objective_func, [])
        while solver_int < len(solvers):
            try:
                prob.solve(solver=solvers[solver_int])
                if not (prob.status == cvx.OPTIMAL or prob.status == cvx.OPTIMAL_INACCURATE):
                    raise cvx.error.SolverError
            except cvx.error.SolverError:
                solver_int += 1
                continue
            break

        self.df['Prediction'] = self.df['Power'] * (scale_var.value + self.df['Cloud'] * cloud_var.value + self.df['Cloud'] ** 2 * cloud_sqr_var.value) + add_var.value + self.df['Power'] ** 2 * scale_sqr_var.value
        self.df['Prediction'][self.df['Prediction'] < 0] = 0

        optimized_values = {'scale_var': float(scale_var.value),
        'scale_sqr_var': float(scale_sqr_var.value),
        'cloud_var': float(cloud_var.value),
        'cloud_sqr_var': float(cloud_sqr_var.value),
        'add_var': float(add_var.value)}

        return optimized_values

    def check_accuracy(self):
        acc = 0
        diff = sum(abs(self.df['Prediction'] - self.df['Ground']))
        ground = sum(abs(self.df['Ground']))
        if ground:
            acc = 1 - (diff / ground)
        return acc

    def check_rmse(self):
        rms = sum(abs((self.df['Prediction'] - self.df['Ground'])**2))
        return rms

    def plot_prediction_and_ground(self):
        ax = plt.gca()

        self.df.plot(kind='area', stacked=False, y='Ground', color='red', ax=ax)
        self.df.plot(kind='area', stacked=False, y='Prediction', ax=ax)

        plt.show()

class APIDataCollector(object):
    """Collects Data from the Flask API."""
    def __init__(self, day, lat, long, base_url = 'http://127.0.0.1:5000'):
        self.base_url = base_url
        self.day = day
        self.lat = lat
        self.long = long
        self.df = pd.DataFrame(index=pd.date_range(day, day + timedelta(1), freq='5min'))

    def fetch_solar_irradaice(self):
        response = urllib.request.urlopen("{}/solar/{}/{}/{}".format(self.base_url, self.day.strftime("%Y-%m-%d"), float(self.lat), float(self.long)))
        data = json.loads(response.read().decode("utf-8"))
        new_dict = {datetime.fromtimestamp(float(k)): v for k, v in data.items()}
        self.df['Solar'] = self.df.index.map(new_dict)

    def fetch_weather_predictions(self):
        for type in ['Cloud', 'Rain']:
            response = urllib.request.urlopen("{}/weather/{}/{}/{}/{}".format(self.base_url, type, self.day.strftime("%Y-%m-%d"), float(self.lat), float(self.long)))
            data = json.loads(response.read().decode("utf-8"))
            new_dict = {datetime.fromtimestamp(float(k)): v for k, v in data[type].items()}
            self.df[type] = self.df.index.map(new_dict)


class Database(object):
    """Used to collect ground truth data on power production from AWS database."""
    def __init__(self, host, port, user, passwd, database):
        self.mydb = mysql.connector.connect(
          host=host,
          port=port,
          user=user,
          passwd=passwd,
          database=database
        )

        self.mycursor = self.mydb.cursor()

    def collect_data(self, mac_address, date, interval):
        date = datetime(date.year, date.month, date.day)
        date = date.replace(tzinfo=pytz.utc)
        start_time = date
        end_time = date + timedelta(1)
        sql = "SELECT Time, PowerApp FROM portal_aggregate_powerdata WHERE device_id = %s AND Time >= %s AND Time <= %s ORDER BY Time ASC"
        val = (mac_address, start_time, end_time,)
        self.mycursor.execute(sql, val)

        res = self.mycursor.fetchall()
        new_dict = {item[0].replace(tzinfo=pytz.utc): item[1] for item in res}

        self.df = pd.DataFrame(index=pd.date_range(start_time, end_time, freq='5min'), columns=['Ground'], dtype='float64')
        self.df['Ground'] = self.df.index.map(new_dict)
        self.df = self.df.sort_index().fillna(0)
        if interval != '5min':
            self.df = self.df.resample(interval).mean()
        return self.df

if __name__ == "__main__":
    day = datetime(2019,5,10)
    lat, long = 51.5734, 0.0724 # Hackney

    # lat, long = 52.1409, -10.2640 # Dingle
    mac = 'b8-27-eb-e2-be-8d' # Hackney
    # mac = 'b8-27-eb-56-34-80' # Dingle (DEAD)
    # mac = 'b8-27-eb-a3-f9-38' # (DEAD)
    # mac = 'b8-27-eb-af-0e-58' # (DEAD)
    # mac = 'b8-27-eb-b0-1e-aa'
    # mac = 'b8-27-eb-d1-19-4b' # (DEAD)
    # mac = 'b8-27-eb-dc-14-e1' # (DEAD)
    # mac = 'b8-27-eb-e5-ca-37' # Dingle end (DEAD)

    interval = '15min'

    PP = PowerPredictor(day, lat, long, mac, interval)
    PP.run_cvx_solver()
    acc = PP.check_accuracy()
    print("Accuracy: {}".format(acc))
    PP.plot_prediction_and_ground()
