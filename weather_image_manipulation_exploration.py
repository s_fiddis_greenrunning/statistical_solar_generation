"""This script was used to try to determine the equations and variables to use
to select a pixel from the MetOffice Forecast Images based on latitude and longitude."""

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

# im1 = Image.open("forcast1.png")
# a1 = np.asarray(im1)
#
# im2 = Image.open("forcast2.png")
# a2 = np.asarray(im2)
#
# im3 = Image.open("forcast3.png")
# a3 = np.asarray(im3)
#
# im4 = Image.open("forcast4.png")
# a4 = np.asarray(im4)
#
# im5 = Image.open("forcast5.png")
# a5 = np.asarray(im5)
#
# im6 = Image.open("forcast6.png")
# a6 = np.asarray(im6)
#
# max = np.maximum(a1, a2)
# max = np.maximum(max, a3)
# max = np.maximum(max, a4)
# max = np.maximum(max, a5)
# max = np.maximum(max, a6)

# print(max.shape)
#
# max_im = Image.fromarray(max)
# max_im.show()

red_im = Image.open("forcast1.png")

red_max = np.array(red_im)
print(red_max.shape)
print(np.max(red_max))
print(set(red_max[:,:,3].flatten()))
print(set(red_max[:,:,2].flatten()))
print(set(red_max[:,:,1].flatten()))
print(set(red_max[:,:,0].flatten()))

# max_im = Image.fromarray(red_max)#[250:,:250])
# max_im.show()

# for i in range(500):
#     if red_max[499, i] > 2:
#         print(i)
#         print(red_max[499,i])
#         break
#
# for i in range(500):
#     if red_max[i,250] > 2:
#         print(i)
#         print(red_max[i,250])
#         break
#
# for i in range(499, 0, -1):
#     if red_max[i,250] > 4:
#         print(i)
#         print(red_max[i,250])
#         break
#
# for i in range(499, 0, -1):
#     if red_max[499, i] > 2:
#         print(i)
#         print(red_max[499, i])
#         break

# Images bounding box is from 48.0 to 61.0 degrees north and 12.0 degrees west to 5.0 degrees east.

# NOTE: Need to create mapping from image to latitude and longitude

x_1 = 0
y_1 = 23.0
x_2 = 250.0
y_2 = 6.0
x_3 = 500.0
y_3 = 15.0

A = x_1*(y_2 - y_3) - y_1*(x_2 - x_3) + x_2*y_3 - x_3*y_2
B = (x_1**2 + y_1**2)*(y_3 - y_2) + (x_2**2 + y_2**2)*(y_1 - y_3) + (x_3**2 + y_3**2)*(y_2 - y_1)
C = (x_1**2 + y_1**2)*(x_2 - x_3) + (x_2**2 + y_2**2)*(x_3 - x_1) + (x_3**2 + y_3**2)*(x_1 - x_2)
D = (x_1**2 + y_1**2)*(x_3*y_2 - x_2*y_3) + (x_2**2 + y_2**2)*(x_1*y_3 - x_3*y_1) + (x_3**2 + y_3**2)*(x_2*y_1 - x_1*y_2)

cir_x = - B / (2 * A)
cir_y = - C / (2 * A)
cir_r = np.sqrt((B**2 + C**2 - 4*A*D)/ (4*A**2))

print(cir_x)
print(cir_y)
print(cir_r)

cir_x = 288.367
cir_y = 2416.961
cir_r = 2411.268

fig = plt.figure()
ax = fig.add_subplot(1,1,1)

# for x in range(50):
# cir_r -= 485
# for i in range(500):
#     for j in range(500):
#         if int(cir_r - np.sqrt((cir_x - i) ** 2 + (cir_y - j) ** 2)) == 0:
#             red_max[j,i] = 0


max_im = Image.fromarray(red_max)#[250:,:250])
max_im.show()

# y = 499.0
# for x in range(40, 480, 10):
#     m = (x - cir_x)/(y - cir_y + 500)
#     c = int(x - (m*y))
#     for i in range(500):
#         for j in range(500):
#             if int(i - (m*j) - c) == 0:
#                 red_max[j, i] = 0
    # m = (y - cir_y + 500)/(x - cir_x)
    # c = int(y - (m*x))
    # for i in range(500):
    #     for j in range(500):
    #         if int(j - (m*i) - c) in [-5,-4,-3,-2,-1,0,1,2,3,4,5]:
    #             red_max[j, i] = 0

m_neg_12 = (43 - cir_x)/(499 - (cir_y - 500))
m_pos_5 = (472 - cir_x)/(499 - (cir_y - 500))
print(m_neg_12)
print(m_pos_5)
m_grad = (m_neg_12 - m_pos_5)/(-12 - 5)
print("m_grad: {}".format(m_grad))
m_c = 12 * m_grad + m_neg_12
print(m_c)

r_61 = 2411.268
r_48 = r_61 - 486
r_grad = (r_61 - r_48)/(61 - 48.0)
r_c = r_61 - r_grad*61

def colour_pixel_at_lat_long(lat, long, red_max):
    grad = long * m_grad + m_c
    print(grad)
    c = cir_x - (grad * (cir_y - 500))
    print(c)
    r = lat * r_grad + r_c
    for x in range(500):
        for y in range(500):
            if int(c + (grad * y) - x) == 0:
                if int(r - np.sqrt((cir_x - x) ** 2 + (cir_y - y) ** 2)) == 0:
                    return red_max[y, x]

    #
    # for i in range(500):
    #     for j in range(500):
    #         if int(r - np.sqrt((cir_x - i) ** 2 + (cir_y - j) ** 2)) == 0:
    #             red_max[j,i] = 0

    # a = (grad ** 2 + 1)
    # b = 2 * (grad*c - grad*cir_x - cir_y)
    # c = (cir_x**2 - r**2 + cir_y**2 - 2*c*cir_x + c**2)
    # x = (-b + np.sqrt(b**2 - 4*a*c)) / 2*a
    # y = int((x - c)/grad)
    # x = int(x)
    # print("x: {}, y: {}".format(x,y))
    #

    # red_max[y,x] = 0

banister_house_lat, banister_house_long = 51.5486, -0.0466

val = colour_pixel_at_lat_long(banister_house_lat, banister_house_long, red_max)

print(val)

max_im = Image.fromarray(red_max)#[250:,:250])
max_im.show()

def determine_radius(x,y):
    return np.sqrt((x - cir_x) ** 2 + (y - cir_y) ** 2)

def get_long_pixel(long):
    return
