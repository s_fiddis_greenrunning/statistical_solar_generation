"""Forecast fetcher retrieves the cloud cover and precipitiation map overlay
forecast images from the MET Offices datapoint API on a daily basis."""

import urllib
from datetime import datetime, timedelta
import os
import sys

CLOUD_STR = "Total_Cloud_Cover"
PRECIP_STR = "Precipitation_Rate"
api_key = "8d55a471-3e89-48f4-a60e-d49e2015ae13"

class ForecastFetcher(object):
    def __init__(self, base_dir='/home/saf16/Desktop/statistical_solar_generation/forecasts', day=datetime.now(), key=api_key):
        self.base_dir = base_dir
        self.day=datetime(day.year, day.month, day.day)
        self.api_key = key
        self.base_url = "http://datapoint.metoffice.gov.uk/public/data/layer/wxfcs/{}/png?RUN={}&FORECAST={}&key={}"

    def get_forecasts(self, type="Cloud"):
        """Retrieves the forecast images from the MET Office API and places it into a directory"""
        directory_path = self.get_directory_path(type)
        forecast_string = self.get_prediction_string(type)
        date_string = self.get_date_string()
        for forecast in range(3,29,3):
            file_name = (self.day + timedelta(hours=forecast-3)).strftime("%Y-%m-%dT%H-%M-%SZ")
            print(self.base_url.format(forecast_string, date_string, forecast, self.api_key))
            urllib.urlretrieve(self.base_url.format(forecast_string, date_string, forecast, self.api_key), os.path.join(directory_path, "{}.png".format(file_name)))

    def get_prediction_string(self, type):
        if type == "Cloud":
            return CLOUD_STR
        elif type == "Rain":
            return PRECIP_STR

    def get_date_string(self):
        date = self.day - timedelta(hours=3)
        return date.strftime("%Y-%m-%dT%H:%M:%SZ")

    def get_directory_path(self, type):
        directory_date_string = self.day.strftime("%Y-%m-%d")
        directory_path = os.path.join(self.base_dir, type, directory_date_string)
        if not os.path.exists(directory_path):
            os.mkdir(directory_path)
        return directory_path

if __name__ == "__main__":
    FF = ForecastFetcher()
    FF.get_forecasts("Cloud")
    FF.get_forecasts("Rain")
