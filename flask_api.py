"""Flask API to serve out Solar and Weather Data."""

from flask import Flask, request, jsonify
import json
from flask_restful import Resource, Api
from weather_prediction import WeatherPredictor
from solar_irradiance import SolarIrradiance
from power_prediction_generation import PowerPredictor
from datetime import datetime
from functools import wraps

app = Flask(__name__)
app.config['SECRET_KEY'] = 'abysmat123'
api = Api(app)

WP = WeatherPredictor()
SI = SolarIrradiance()

def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = request.args.get('token')

class WeatherPredictions(Resource):
    def get(self, type, date, lat, long):
        if type in ['Cloud', 'Rain']:
            return jsonify(WP.get_interpolated_predictions(date, lat, long, type))
        else:
            return {}, 404

class SolarIrradiance(Resource):
    def get(self, date, lat, long):
        return jsonify(SI.produce_solar_curve(date, lat, long))

class PowerPrediction(Resource):
    def get(self, day, lat, long, mac, interval='60min', day_range=5):
        PP = PowerPredictor(day, lat, long, mac, interval, day_range)
        return json.dumps(PP.run_cvx_solver())

api.add_resource(WeatherPredictions, '/weather/<string:type>/<string:date>/<float:lat>/<float:long>')
api.add_resource(SolarIrradiance, '/solar/<string:date>/<float:lat>/<float:long>')
api.add_resource(PowerPrediction, '/powerprediction/<string:day>/<float:lat>/<float:long>/<string:mac>/<string:interval>/<int:day_range>')

if __name__ == '__main__':
    app.run(debug=True)
