from power_prediction_generation import PowerPredictor
import pytest
from datetime import datetime
import json

@pytest.fixture
def power_predictor():
    """Returns a fresh power predictor class"""
    day = datetime(2019,4,21)
    lat, long = 50, 0
    mac = 'b8-27-eb-e2-be-8d'
    return PowerPredictor(day, lat, long, mac, '5min', 3)

@pytest.mark.parametrize("scale_var,scale_sqr_var,cloud_var,cloud_sqr_var,add_var", [
(2, 0.1245, -0.52, 0.32, 80),
(3, 0.534, 842, -9.3, 0.12)
])
def test_optimization(power_predictor, scale_var, scale_sqr_var, cloud_var, cloud_sqr_var, add_var):
    power_predictor.setup_dummy_ground(scale_var, scale_sqr_var, cloud_var, cloud_sqr_var, add_var)
    test_values = {'scale_var': scale_var, 'scale_sqr_var': scale_sqr_var, 'cloud_var': cloud_var, 'cloud_sqr_var': cloud_sqr_var, 'add_var': add_var}
    res_values = power_predictor.run_cvx_solver()
    for k, _ in test_values.items():
        assert test_values[k] == pytest.approx(res_values[k])
