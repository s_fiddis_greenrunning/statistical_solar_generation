import pytest
from ..solar_irradiance import SolarIrradiance
from datetime import datetime
import json
import os

@pytest.fixture
def solar_irradiance():
    """Returns a fresh solar irradiance class"""
    return SolarIrradiance()

@pytest.mark.parametrize("day,lat,long", [
(datetime(2019,4,4), 50, 0),
("2019-04-04", 50, 0)
])
def test_produce_solar_curve(solar_irradiance, day, lat, long):
    res = solar_irradiance.produce_solar_curve(day, lat, long)
    expected = _open_test_file(day, lat, long)
    assert res == expected

def _open_test_file(day, lat, long):
    if isinstance(day, datetime):
        day = day.strftime('%Y-%m-%d')
    filename = os.path.join('tests','predictions','solar','{}_{}_{}.json'.format(day, lat, long))
    with open(filename, 'r') as file:
        expected = json.load(file)
        expected = json.loads(expected)
    return expected
