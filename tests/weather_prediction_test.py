from ..weather_prediction import WeatherPredictor
import pytest
from datetime import datetime
import json
import os

@pytest.fixture
def weather_predictor():
    """Returns a fresh weather predictor class"""
    return WeatherPredictor()

@pytest.mark.parametrize("lat,long,day,type,expected", [
    (50.0, 0.0, datetime(2019,4,4), "Cloud", 1.0),
    (60.0, -6.0, datetime(2019,4,4), "Cloud", 1.0),
    (60.0, -6.0, datetime(2019,4,4), "Rain", 0),
    (60.0, -10.0, datetime(2019,4,4), "Rain", 0),
])
def test_get_prediction(weather_predictor, lat, long, day, type, expected):
    res = weather_predictor.get_prediction(day, lat, long, type)
    assert res == expected

@pytest.mark.parametrize("day,lat,long,type,interval", [
    ("2019-04-04",50,0,"Cloud","5min"),
    (datetime(2019,4,4),50,0,"Cloud","5min")
])
def test_get_interpolated_predictions(weather_predictor, day, lat, long, type, interval):
    res = weather_predictor.get_interpolated_predictions(day, lat, long, type, interval)

    expected = _open_test_file(day, lat, long, type, interval)

@pytest.mark.parametrize("lat,long,x,y", [
    (60, -10, 32, 56),
    (50, 0, 349, 417),
])
def test_determine_x_y(weather_predictor, lat, long, x, y):
    res = weather_predictor._determine_x_y(lat, long)
    assert res == (x, y)

def _open_test_file(day, lat, long, type, interval):
    if isinstance(day, datetime):
        day = day.strftime('%Y-%m-%d')
    filename = os.path.join('tests','predictions',type.lower(),'{}_{}_{}_{}.json'.format(day, lat, long, interval))
    with open(filename, 'r') as file:
        expected = json.load(file)
    return expected
