from datetime import datetime, timedelta
import json
import math
import pytz
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pysolar import solar, radiation

class SolarIrradiance:
    """Uses the pysolar library to produce time series data on the Azimuth, Elevation and
    Ground Level Solar Radiation for a given day, latitude and longitude.
    """
    def produce_solar_dataframe(self, day, lat, long, interval='5min'):
        """Produces a pandas dataframe containing the Suns Azimuth, Elevation and
        the Ground Level Solar Radiation for a given day, latitude and longitude
        with a period defined by the interval parameter."""
        if isinstance(day, str):
            day = datetime.strptime(day, '%Y-%m-%d')
            day = day.replace(tzinfo=pytz.utc)
        start_time = datetime(day.year, day.month, day.day)
        start_time = start_time.replace(tzinfo=pytz.utc)
        end_time = start_time + timedelta(1)
        df = pd.DataFrame(index=pd.date_range(start_time, end_time, freq=interval))
        df['Azimuth'], df['Elevation'] = np.vectorize(solar.get_position)(lat, long, df.index.to_pydatetime())
        df['Solar'] = np.vectorize(radiation.get_radiation_direct)(df.index.to_pydatetime(), df['Elevation'])
        return df

    def produce_solar_curve(self, day, lat, long):
        """Creates data to be sent via Flask API."""
        df = self.produce_solar_dataframe(day, lat, long)
        json_string = df['Solar'].to_json(date_format='epoch', date_unit='s', double_precision=4)
        return json.loads(json_string)

if __name__ == "__main__":
    day = datetime.now()
    day = datetime(2019,4,4)
    day = day.replace(tzinfo=pytz.utc)

    hack_lat, hack_long = 51.5486, -0.0466
    al_lat, al_long = 52.5612, 1.7294
    lat, long = 50, 0

    SI = SolarIrradiance()

    solar_df = SI.produce_solar_curve(day, lat, long)

    import json

    with open('{}-{}-{}.json'.format(day.strftime('%Y-%m-%d'), lat, long), 'w') as outfile:
        json.dump(solar_df, outfile)

    print(solar_df)
