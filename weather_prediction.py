from datetime import datetime, timedelta
from PIL import Image
import numpy as np
import pandas as pd
import sys
import json
import pytz
import matplotlib.pyplot as plt
import os

# Variables to undo mercator projection
cir_x = 288.367
cir_y = 2416.961
cir_r = 2411.268

m_neg_12 = 0.173042135856
m_pos_5 = -0.129504972281
m_grad = -0.0177968887139
m_c = -0.0405205287111

r_61 = 2411.268
r_48 = r_61 - 486
r_grad = (r_61 - r_48)/(61 - 48.0)
r_c = r_61 - r_grad * 61

MULT_LIST = [100,10,1,0]

rainfall_dic = {}
rainfall_dic[0] = 0
rainfall_dic[sum([a*b for a,b in zip(MULT_LIST,[0,0,254,255])])] = 0.5
rainfall_dic[sum([a*b for a,b in zip(MULT_LIST,[50,101,254,255])])] = 1.0
rainfall_dic[sum([a*b for a,b in zip(MULT_LIST,[127,127,0,255])])] = 2.0
rainfall_dic[sum([a*b for a,b in zip(MULT_LIST,[254,203,0,255])])] = 4.0
rainfall_dic[sum([a*b for a,b in zip(MULT_LIST,[254,152,0,255])])] = 8.0
rainfall_dic[sum([a*b for a,b in zip(MULT_LIST,[254,0,0,255])])] = 16.0
rainfall_dic[sum([a*b for a,b in zip(MULT_LIST,[254,0,254,255])])] = 32.0
rainfall_dic[sum([a*b for a,b in zip(MULT_LIST,[229,254,254,255])])] = 64.0

class WeatherPredictor(object):
    def __init__(self):
        pass

    def get_daily_predictions(self, day, lat, long, type="Cloud"):
        day = datetime(day.year, day.month, day.day)
        day = day.replace(tzinfo=pytz.utc)
        predictions = {}
        for h in range(0,24,3):
            time = day + timedelta(hours=h)
            predictions[time] = self.get_prediction(time, lat, long, type)
        return predictions

    def get_prediction(self, time, lat, long, type):
        img = self.fetch_image(time, type)
        return self.get_forecast(img, lat, long, type)

    def fetch_image(self, time, type):
        img_path = os.path.join("forecasts",str(type), time.strftime("%Y-%m-%d"), time.strftime("%Y-%m-%dT%H-%M-%SZ") + ".png")
        img = Image.open(img_path)
        return np.array(img)

    def get_forecast(self, img, lat, long, type="Cloud"):
        x, y = self._determine_x_y(lat, long)

        if type == "Cloud":
            return img[y, x, 3] / 255.0
        elif type == "Rain":
            return rainfall_dic[sum([a*b for a,b in zip(MULT_LIST,img[y, x])])]
        else:
            return NotImplementedError

    def _determine_x_y(self, lat, long):
        # TODO: Efficiency can be greatly improved by determining the intersection of both equations dirrectly.
        grad = long * m_grad + m_c
        c = cir_x - (grad * (cir_y - 500))
        r = lat * r_grad + r_c

        B = 2 * ((cir_x - c)*grad + cir_y)
        A = (1 + grad**2)
        C = ((cir_x - c)**2 + cir_y**2 - r**2)
        y = -(-B + np.sqrt(B**2 - (4*A*C))) / (2*A)
        x = y * grad + c
        y = int(y)
        x = int(x)

        return x, y

    def get_interpolated_predictions(self, day, lat, long, type="Cloud", interval="5min"):
        df = self.get_interpolated_dataframe(day, lat, long, type, interval)
        json_string = df.to_json(date_format='epoch', date_unit='s', double_precision=4)
        return json.loads(json_string)

    def get_interpolated_dataframe(self, day, lat, long, type="Cloud", interval="5min"):
        if isinstance(day, str):
            day = datetime.strptime(day, '%Y-%m-%d')
        day = datetime(day.year, day.month, day.day)
        day = day.replace(tzinfo=pytz.utc)
        predictions = self.get_daily_predictions(day, lat, long, type)
        df = pd.DataFrame(index=pd.date_range(day, day + timedelta(1), freq=interval), columns=[type], dtype='float64')
        for k, v in predictions.items():
            df.at[k, type] = v
        df[type] = df[type].interpolate()
        return df

if __name__ == "__main__":
    WP = WeatherPredictor()
    day = datetime(2019,3,29)
    hack_lat, hack_long = 51.5486, -0.0466
    al_lat, al_long = 52.5612, 1.7294
    lat, long = 60.0, -10.0
    day = datetime(2019,4,1)
    preds = WP.get_daily_predictions(datetime.now(), hack_lat, hack_long, 'Cloud')
    pred_df = WP.get_interpolated_predictions(day, hack_lat, hack_long, 'Cloud', '30min')

    lists = sorted(pred_df["Cloud"].items())
    x, y = zip(*lists)
    x = map(lambda d: datetime.utcfromtimestamp(int(d)), x)

    plt.plot(x, y)
    plt.xlabel('Date')
    plt.ylabel('Cloud Cover Ratio')
    plt.title('Cloud Cover')
    plt.show()
